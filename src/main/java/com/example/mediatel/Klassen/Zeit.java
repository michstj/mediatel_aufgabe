package com.example.mediatel.Klassen;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Zeit {
    public String getZeit(){
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        String date1 = formatter.format(date);
        return  date1;
    }
}
