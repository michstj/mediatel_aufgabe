package com.example.mediatel.Exceptions;

public class CustomerNotFoundException extends Exception {
    public CustomerNotFoundException(){
        super("Employee kann nicht angelegt werden, da der Customer nicht vorhanden ist!");
    }
}
