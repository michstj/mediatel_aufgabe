package com.example.mediatel.Exceptions;

public class DoubleEmployeeEmailException extends Exception {
    public DoubleEmployeeEmailException(){
        super("Email bereits in DB vorhanden!");
    }
}
