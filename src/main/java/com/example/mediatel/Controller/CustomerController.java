package com.example.mediatel.Controller;

import com.example.mediatel.Model.Customer;
import com.example.mediatel.Services.CustomerService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class CustomerController
{
    private CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("/customer/add")
    public void addCustomer(@RequestBody Customer customer){
        customerService.addCustomer(customer);
    }

    @DeleteMapping("/customer/{id}/delete")
    public void deleteCustomer(@PathVariable Long id){
        customerService.deleteCustomer(id);
    }

    @PutMapping("/customer/{id}/update")
    public void updateCustomer(@PathVariable Long id, @RequestBody @Valid Customer customer){

        customerService.updateCustomer(id, customer);
    }

    @GetMapping("/customer/{id}")
    public ResponseEntity<Customer>getCustomer(@PathVariable Long id){
        return ResponseEntity.ok((customerService.getCustomer(id)));
    }
}
