package com.example.mediatel.Controller;

import com.example.mediatel.Exceptions.CustomerNotFoundException;
import com.example.mediatel.Exceptions.DoubleEmployeeEmailException;
import com.example.mediatel.Exceptions.ExceptionResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionController {
    @ExceptionHandler(DoubleEmployeeEmailException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public ResponseEntity<ExceptionResponseEntity> handleDuplicateEmailEmployee (DoubleEmployeeEmailException ex){
        ExceptionResponseEntity exre = new ExceptionResponseEntity("1000", ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(CustomerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity<ExceptionResponseEntity> handleCustomerNotFound (CustomerNotFoundException ex){
        ExceptionResponseEntity exre = new ExceptionResponseEntity("999", ex.getMessage());
        return new ResponseEntity<>(exre, HttpStatus.NOT_FOUND);
    }
}
