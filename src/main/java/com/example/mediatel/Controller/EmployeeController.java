package com.example.mediatel.Controller;

import com.example.mediatel.Exceptions.CustomerNotFoundException;
import com.example.mediatel.Exceptions.DoubleEmployeeEmailException;
import com.example.mediatel.Model.Customer;
import com.example.mediatel.Model.Employee;
import com.example.mediatel.Services.EmployeeService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class EmployeeController {

    private EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }


    //Im RequestBody Vorname, Nachname und Email mitgeben und in der PathVariable "id" die Id des Customers mitgeben, dem der Employee zugeordnet werden soll
    @PostMapping("/employee/add/to/{id}")
    public ResponseEntity<Employee> addEmployee(@PathVariable Long id, @RequestBody Employee employee) throws CustomerNotFoundException, DoubleEmployeeEmailException{
       return ResponseEntity.ok(employeeService.addEmployeeToCustomer(employee, id));
    }

    @DeleteMapping("/employee/{id}/delete")
    public void deleteEmployee(@PathVariable Long id){
        employeeService.deleteEmployee(id);
    }

    @PutMapping("/employee/{id}/update")
    public void updateEmployee(@PathVariable Long id, @RequestBody @Valid Employee employee){

        employeeService.updateEmployee(id, employee);
    }

    @GetMapping("/employee/{id}")
    public ResponseEntity<Employee> getEmployee(@PathVariable Long id){
        return ResponseEntity.ok((employeeService.getEmployee(id)));
    }
}
