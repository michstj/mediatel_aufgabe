package com.example.mediatel;

import com.example.mediatel.Model.Customer;
import com.example.mediatel.Model.Employee;
import com.example.mediatel.Repositories.CustomerRepository;
import com.example.mediatel.Repositories.EmployeeRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MediatelApplication {

	public static void main(String[] args) {
		SpringApplication.run(MediatelApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository customerRepository, EmployeeRepository employeeRepository) {
		//soll beim Start zum Bootstrapping ausgeführt werden.
		return (args) -> {
			Customer c1 = new Customer();
			c1.setCreationDate("2019-11-12");
			c1.setUpdateDate("2019-11-12");
			c1.setName("firma GmbH");
			c1.setZip("6380");
			c1.setCity("St. Johann in Tirol");
			c1.setWebsite("www.example.com");
			customerRepository.save(c1);

			Employee e1 = new Employee();
			e1.setCreationDate("2019-11-12");
			e1.setUpdateDate("2019-11-12");
			e1.setCompany(c1);
			e1.setEmail("asdf@asd.at");
			e1.setFirstname("Michael");
			e1.setLastname("Stjepic");
			employeeRepository.save(e1);

		};

	}

}
