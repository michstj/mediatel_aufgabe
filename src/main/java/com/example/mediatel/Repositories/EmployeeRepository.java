package com.example.mediatel.Repositories;

import com.example.mediatel.Model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
        Employee findByEmail(String email);
}
