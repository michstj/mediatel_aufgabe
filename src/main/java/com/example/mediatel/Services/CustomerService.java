package com.example.mediatel.Services;

import com.example.mediatel.Klassen.Zeit;
import com.example.mediatel.Model.Customer;
import com.example.mediatel.Repositories.CustomerRepository;
import com.example.mediatel.Repositories.EmployeeRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
public class CustomerService {

    CustomerRepository customerRepository;
    EmployeeRepository employeeRepository;
    Zeit zeit = new Zeit();


    public CustomerService(CustomerRepository customerRepository, EmployeeRepository employeeRepository){
        this.customerRepository = customerRepository;
        this.employeeRepository = employeeRepository;
    }

    public void addCustomer(Customer customer){

        //CreationDate und UpdateDate auf heutiges Datum setzen
        customer.setCreationDate(zeit.getZeit());
        customer.setUpdateDate(zeit.getZeit());

        customerRepository.save(customer);
    }

    public void deleteCustomer(Long id){
        Optional<Customer> customer = customerRepository.findById(id);
        if(customer.isPresent()){
            customerRepository.delete(customer.get());
        }
    }

    public void updateCustomer(Long id, Customer customer){
        Optional<Customer> optionalCustomer = customerRepository.findById(id);
        if(optionalCustomer.isPresent()){
            Customer customer1;
            customer1 = customer;
            customer1.setUpdateDate(zeit.getZeit());
            customerRepository.save(customer1);
        }
    }

    public Customer getCustomer(Long id){
        Optional<Customer> customer = customerRepository.findById(id);
        if (customer.isPresent()){
            return customer.get();
        }
        return null;
    }


}
