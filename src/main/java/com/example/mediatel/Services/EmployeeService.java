package com.example.mediatel.Services;

import com.example.mediatel.Exceptions.CustomerNotFoundException;
import com.example.mediatel.Exceptions.DoubleEmployeeEmailException;
import com.example.mediatel.Klassen.Zeit;
import com.example.mediatel.Model.Customer;
import com.example.mediatel.Model.Employee;
import com.example.mediatel.Repositories.CustomerRepository;
import com.example.mediatel.Repositories.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class EmployeeService {

    CustomerRepository customerRepository;
    EmployeeRepository employeeRepository;
    Zeit zeit = new Zeit();


    public EmployeeService(CustomerRepository customerRepository, EmployeeRepository employeeRepository){
        this.customerRepository = customerRepository;
        this.employeeRepository = employeeRepository;
    }

    public Employee addEmployeeToCustomer(Employee employee, Long id) throws CustomerNotFoundException, DoubleEmployeeEmailException{

        //CreationDate und UpdateDate auf heutiges Datum setzen
        employee.setCreationDate(zeit.getZeit());
        employee.setUpdateDate(zeit.getZeit());

        Optional<Customer> opCust = customerRepository.findById(id);
        if(opCust.isPresent()){
            Customer customer = opCust.get();
            employee.setCompany(customer);

            if(employeeRepository.findByEmail(employee.getEmail()) == null){
                employeeRepository.save(employee);
            }
            else {
                throw new DoubleEmployeeEmailException();
            }

            return employee;
        }
        else {
            throw new CustomerNotFoundException();
        }


    }

    public void deleteEmployee(Long id){
        Optional<Employee> employee = employeeRepository.findById(id);
        if(employee.isPresent()){
            employeeRepository.delete(employee.get());
        }
    }

    public Employee updateEmployee(Long id, Employee employee){
        Optional<Employee> opEmp = employeeRepository.findById(employee.getId());
        if(opEmp.isPresent()){
            Employee employee1 = opEmp.get();
            employee1.setUpdateDate(zeit.getZeit());

                employee1.setFirstname(employee.getFirstname());
                employee1.setLastname(employee.getLastname());
                employee1.setEmail(employee.getEmail());

            return employeeRepository.save(employee1);
        }

        return employeeRepository.save(employee);

    }

    public Employee getEmployee(Long id){
        Optional<Employee> employee = employeeRepository.findById(id);
        if (employee.isPresent()){
            return employee.get();
        }
        return null;
    }
}
